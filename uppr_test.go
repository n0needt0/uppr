/**
Copyright 2015 andrew@yasinsky.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package uppr

import (
	"bufio"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"os"
	//"sync"
	"testing"
	"time"
)

func TestLogFile(t *testing.T) {

	//these two we need for sure
	logfile := "./uppr.log"
	loglevel := "DEBUG"

	//delete log file bofore test
	os.Remove(logfile)

	//you must get log before using it
	log := GetLog()
	//do all the wiring up
	//NewUppr terurns http handle function func(http.ResponseWriter, *http.Request)
	// use it like this http.Handle("/loglevel", http.HandlerFunc(changeLogLevelHandle))
	//make sure you do it BEFORE you start server

	changeLogLevelHandle := New(logfile, loglevel)

	//this is test we add 5 lines to log file
	i := 0
	for i < 5 {
		time.Sleep(time.Duration(100) * time.Millisecond)
		log.Debug(fmt.Sprintf("%s", time.Now().Format(time.UnixDate)))
		i++
	}
	//now we make sure we have 5 lines of code

	file, err := os.Open(logfile)
	if err != nil {
		panic(err)
	}

	fileScanner := bufio.NewScanner(file)
	lineCount := 0
	for fileScanner.Scan() {
		lineCount++
	}
	assert.Equal(t, 5, lineCount, "")

	//attach handle
	http.Handle("/loglevel", http.HandlerFunc(changeLogLevelHandle))

	//here we start server on localhost:6060
	//http.ListenAndServe("localhost:6161", nil)

	fmt.Println("if you go to curl http://localhost:6060/?set=debug")
	//both log file at /tmp/uppr.log and curl out put
	//will show
	//Setting log level to: DEBUG
	// Valid log levels are CRITICAL, ERROR,  WARNING, NOTICE, INFO, DEBUG

	//wg := &sync.WaitGroup{}
	//wg.Add(1)
	//wg.Wait()

}
