/**
Copyright 2015 andrew@yasinsky.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package uppr

import (
	"fmt"
	logging "github.com/op/go-logging"
	"github.com/unrolled/render"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

var (
	LOGFMT = "%{color}%{time:15:04:05.000000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}"
	LOGGER = "uppr"
)

var done = make(chan struct{})

func GetLog() *logging.Logger {
	return logging.MustGetLogger(LOGGER)
}

//new uppr initializes loggers and return handle function for HTTPListenand Serve
//Attach it to any Server and call it as server /url?level=LOGLEVEL
func New(logfile string, loglevel string) func(http.ResponseWriter, *http.Request) {
	if logfile == "" || loglevel == "" {
		logfile = "STDOUT"
		loglevel = "DEBUG"
	}

	log := logging.MustGetLogger(LOGGER)

	logFormat := logging.MustStringFormatter(LOGFMT)

	var logback *logging.LogBackend

	loglevel = strings.ToUpper(loglevel)

	if logfile == "STDOUT" {
		logback = logging.NewLogBackend(os.Stdout, "", 0)
	} else if logfile == "STDERR" {
		logback = logging.NewLogBackend(os.Stderr, "", 0)
	} else {

		os.MkdirAll(filepath.Dir(logfile), 0777)

		logFile, err := os.OpenFile(logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("Log file error: %s %s", logfile, err)
		}

		logback = logging.NewLogBackend(logFile, "", 0)
	}

	logformatted := logging.NewBackendFormatter(logback, logFormat)

	logging.SetBackend(logformatted)

	if loglvl, err := logging.LogLevel(loglevel); err == nil {
		logging.SetLevel(loglvl, "")
	} else {
		logging.SetLevel(logging.DEBUG, "")
	}

	//register Signals to kill app ctl+C or on process exit/kill
	killsignals := []os.Signal{os.Interrupt, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT}
	killappch := make(chan os.Signal, 1)
	signal.Notify(killappch, killsignals...)

	go func() {
		<-killappch
		log.Fatalf("Interrupted: %s", time.Now().Format(time.UnixDate))
	}()

	return func(w http.ResponseWriter, r *http.Request) {
		log := logging.MustGetLogger(LOGGER)
		loglevel := strings.ToUpper(r.FormValue("level"))
		newlevel, err := logging.LogLevel(loglevel)
		if err != nil {
			newlevel = logging.DEBUG
			loglevel = "DEBUG"
		}

		res := map[string]string{
			"result": fmt.Sprintf("Setting log level to: %s", loglevel),
			"usage":  "/loglevel/level={newlevel}",
			"valid":  "CRITICAL, ERROR, WARNING, NOTICE, INFO, DEBUG",
		}

		log.Debug(fmt.Sprintf("%+v", res))
		logging.SetLevel(newlevel, "")
		rndr := render.New(render.Options{
			IndentJSON: true,
		})
		rndr.JSON(w, http.StatusOK, res)
		return
	}
}
